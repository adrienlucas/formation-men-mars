Le formulaire de contact n'est accessible que :
 - un chiffre aléatoire entre 1 et 10 est supérieur à 5
 - la minute courrante est pair
 
Si le formulaire n'est pas accéssible, alors, j'affiche la raison de son indisponibilité
