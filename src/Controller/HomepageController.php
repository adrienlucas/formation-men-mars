<?php

declare(strict_types=1);

namespace App\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController
{
    /**
     * @Route("/hello/{name}", requirements={"name":"[a-zA-Z]+"}, name="app_homepage_index")
     */
    public function index(string $name = 'world'): Response
    {
        return new Response('<html><body>Hello '.$name.' !</body></html>');
    }

    /**
     * @Route("/api")
     */
    public function someApi(): Response
    {
        return new JsonResponse(['someadata' => 'foobar']);
    }
}
