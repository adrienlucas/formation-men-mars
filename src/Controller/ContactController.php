<?php

declare(strict_types=1);

namespace App\Controller;


use App\Contact\ContactFormAvailableService;
use App\Contact\Exception\MinuteIsEvenException;
use App\Contact\Exception\RandomIsBelowFiveException;
use App\Form\ContactType;
use App\Model\Contact;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

class ContactController
{
    /** @var EngineInterface */
    private $templating;
    /** @var FormFactoryInterface */
    private $formFactory;
    /** @var RouterInterface */
    private $router;

    public function __construct(EngineInterface $templating, FormFactoryInterface $formFactory, RouterInterface $router)
    {
        $this->templating = $templating;
        $this->formFactory = $formFactory;
        $this->router = $router;
    }

    /**
     * @Route("/contact")
     */
    public function newContact(Request $request): Response
    {
        $contactFormAvailableService = new ContactFormAvailableService();
        try {
            $contactFormAvailableService->shouldBeAvailable();
        } catch(RandomIsBelowFiveException $e) {
            $templateName = 'contact/disabled_by_random.html.twig';
        } catch(MinuteIsEvenException $e) {
            $templateName = 'contact/disabled_by_even_minutes.html.twig';
        } finally {
            if(isset($templateName)) {
                return new Response($this->templating->render($templateName));
            }
        }

        $contact = new Contact();
        $contactForm = $this->formFactory->create(ContactType::class, $contact);
        $contactForm->handleRequest($request);

        if($contactForm->isSubmitted() && $contactForm->isValid()) {
            return new RedirectResponse($this->router->generate('app_contact_submitsuccess'));
        }

        $pageContent = $this->templating->render(
            'contact/new.html.twig',
            [
                'contactForm' => $contactForm->createView()
            ]
        );
        return new Response($pageContent);
    }

    /**
     * @Route("/contact-success", name="app_contact_submitsuccess")
     */
    public function submitSuccess(): Response
    {
        return new Response('Success !');
    }
}
