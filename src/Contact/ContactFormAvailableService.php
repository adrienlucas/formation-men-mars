<?php

declare(strict_types=1);

namespace App\Contact;


use App\Contact\Exception\MinuteIsEvenException;
use App\Contact\Exception\RandomIsBelowFiveException;

class ContactFormAvailableService
{
    /**
     * @throws MinuteIsEvenException
     * @throws RandomIsBelowFiveException
     */
    public function shouldBeAvailable(): void
    {
        if(rand(1,10) < 5) {
            throw new RandomIsBelowFiveException();
        }

        if(intval(date('i')) % 2) {
            throw new MinuteIsEvenException();
        }
    }
}
